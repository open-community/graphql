from pip._internal.req import parse_requirements
from pip._internal.download import PipSession
from setuptools import setup, find_packages

VERSION = (0, 2, 2)

requirements = [
    str(r.req) for r in
    parse_requirements('requires/installation.txt', session=PipSession())
]

setup(
    name='community_graphql',
    version='.'.join(str(i) for i in VERSION),
    description='community graphql service',
    url='http://github.com/storborg/community_graphql',
    author='Matthew Prestifilippo',
    author_email='m.prestifilippo@gmail.com',
    license='MIT',
    packages=find_packages(),
    install_requires=requirements,
    zip_safe=False,
    entry_points={
        'console_scripts': [
            'community_graphql=community_graphql.server:main'
        ]
    }
)
