FROM python:alpine3.7

COPY ./setup.py /src/setup.py
COPY ./community_graphql /src/community_graphql
COPY ./requires /src/requires

RUN cd /src && python setup.py install

EXPOSE 80

ENTRYPOINT ["community_graphql"]
