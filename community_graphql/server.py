import graphene
import json
import os
import tornado.ioloop
import tornado.web


class Handler(tornado.web.RequestHandler):

    def initialize(self, schema):
        self.schema = schema

    async def get(self):
        return await self.handle()

    async def post(self):
        return await self.handle()

    async def handle(self):
        body = self.parse_body()
        res = self.schema.execute(body['query'], variable_values=body.get('variables'))
        self.set_header('Content-Type', 'application/json')
        return self.write(json.dumps({'data': res.data}))

    @property
    def content_type(self):
        return self.request.headers.get('Content-Type', 'text/plain').split(';')[0]

    def parse_body(self):
        content_type = self.content_type

        if content_type == 'application/graphql':
            return {'query': self.request.body}

        elif content_type == 'application/json':
            try:
                body = self.request.body
            except Exception as e:
                raise ExecutionError(400, e)

            try:
                return json.loads(body)
            except AssertionError as e:
                raise HTTPError(status_code=400, log_message=str(e))
            except (TypeError, ValueError):
                raise HTTPError(status_code=400, log_message='POST body sent invalid JSON.')

        elif content_type in ['application/x-www-form-urlencoded', 'multipart/form-data']:
            return self.request.query_arguments

        return {}



class Query(graphene.ObjectType):
    hello = graphene.String(name=graphene.String(default_value="stranger"))

    def resolve_hello(self, info, name):
        return 'Hello ' + name


def make_app():
    schema = graphene.Schema(query=Query)
    return tornado.web.Application([
        (r"/", Handler, {'schema': schema}),
    ])

def main():
    app = make_app()
    app.listen(os.environ.get('GRAPHQL_PORT', 80))
    tornado.ioloop.IOLoop.current().start()
