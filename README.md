# Community GraphQL

GraphQL server for the community project

## Getting started

* Clone repo
* docker build . -t mprestifilippo/community-graphql:latest
* docker run -p 80 mprestifilippo/community-graphql:latest
